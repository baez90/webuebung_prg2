package ro.inf.prg2.webUebung.test;

import org.junit.Test;
import ro.inf.prg2.webUebung.model.Animal;
import ro.inf.prg2.webUebung.model.AnimalFactory;
import ro.inf.prg2.webUebung.model.PlayGround;

import java.util.Iterator;

import static org.junit.Assert.assertTrue;

/**
 * Created by baez on 15.06.15.
 */
public class PlayGroundTest {

    @Test
    public void testPlayGroundRemove() {
        PlayGround<Animal> animalPlayGround = createTestPlayGround();
        Iterator<Animal> playgroundIterator = animalPlayGround.iterator();
        while (playgroundIterator.hasNext()) {
            Animal a = playgroundIterator.next();
            assertTrue(animalPlayGround.remove(a));
        }
    }

    @Test
    public void testTick() {
        PlayGround<Animal> animalPlayGround = createTestPlayGround();
        int sleepingTime = animalPlayGround.getElement(0, 0).getTimeSleeping();
        int digestingTime = animalPlayGround.getElement(0, 0).getTimeDigesting();
        for (int i = 0; i < sleepingTime; i++) {
            animalPlayGround.tick();
        }
        animalPlayGround.forEach(elem -> {
            elem.feed();
            assertTrue(elem.isDigesting());
        });

        for (int i = 0; i < digestingTime; i++) {
            animalPlayGround.tick();
        }

        animalPlayGround.forEach(elem -> {
            assertTrue(elem.collect() > 0);
        });
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPlayGroundConstructor() {
        new PlayGround<Animal>(0, 0);
    }

    private PlayGround<Animal> createTestPlayGround() {
        PlayGround<Animal> testPlayGround = new PlayGround<>(5, 5);
        for (int i = 0; i < 25; i++) {
            int x = i / 5;
            int y = i % 5;
            testPlayGround.place(x, y, AnimalFactory.createLion());
        }
        return testPlayGround;
    }
}
