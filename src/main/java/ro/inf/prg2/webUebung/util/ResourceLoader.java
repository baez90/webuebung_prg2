package ro.inf.prg2.webUebung.util;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

/**
 * Helper class which uses the ClassLoader to load resources from the local resource folder to serve them as binary or string within the api classes
 *
 * @author Peter Kurfer
 * @version 0.1
 */
public class ResourceLoader {

    /**
     * Uses the ClassLoader and a Reader to read the specified file in peaces of 4096 chars
     *
     * @param filePath relative path to the local file
     * @return content of the specified file as string or an empty string if file doesn't exist
     */
    public static String loadResource(String filePath) {
        StringBuilder builder = new StringBuilder();
        ClassLoader loader = Thread.currentThread().getContextClassLoader();

        Reader reader = new InputStreamReader(loader.getResourceAsStream(filePath));

        char[] chars = new char[4 * 1024];
        int len;

        try {
            while ((len = reader.read(chars)) >= 0) {
                builder.append(chars, 0, len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return builder.toString();
    }

    /**
     * Uses the ClassLoader and a part of Apache Commmons IO to read binary files from the local resource folder
     *
     * @param filePath relative path to the local file
     * @return byte content of the specified file, if file doesn't exist an empty array will returned
     */
    public static byte[] loadBinaryResource(String filePath) {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();

        try {
            return IOUtils.toByteArray(loader.getResourceAsStream(filePath));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new byte[0];
    }
}
