package ro.inf.prg2.webUebung.api;

import org.reflections.Reflections;

import javax.ws.rs.Path;
import java.util.HashSet;
import java.util.Set;

/**
 * Helper class which uses reflection library to find all api classes
 *
 * @author Peter Kurfer
 * @version 0.1
 */
public class ApiRegistry {

    public static Set<Class<?>> ApiClasses;

    static {
        ApiClasses = new HashSet<>();
        Reflections reflections = new Reflections("ro.inf.prg2.webUebung.api");
        ApiClasses = reflections.getTypesAnnotatedWith(Path.class);
    }
}
