package ro.inf.prg2.webUebung.api;

import ro.inf.prg2.webUebung.model.*;
import ro.inf.prg2.webUebung.web.HtmlBase;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import java.util.Random;

/**
 * Main API to serve interfaces to create new animals, grounds and decorations and to interact with them
 *
 * @author Peter Kurfer
 * @version 0.1
 */
@Path("/link")
public class HyperlinksApi {

    private Zoo currentZoo = HtmlBase.getInstance().getZoo();
    private Site currentSite = HtmlBase.getInstance().getZoo().getSite();
    private Random rand = new Random();

    /**
     * API to enable animal lifecycle by a tick every second through an ajax call
     *
     * @return empty Response object
     */
    @Path("/tick")
    @GET
    public Response tick() {
        currentSite.tick();
        return Response.ok().build();
    }

    /**
     * API to create a new lion and place it on a free field
     *
     * @return empty Response object
     */
    @Path("/newLion")
    @GET
    public Response placeNewLion() {
        placeZooElement(AnimalFactory.createLion());
        return Response.ok().build();
    }

    /**
     * API to create a new gazella and place it on a free field
     *
     * @return empty Response object
     */
    @Path("/newGazella")
    @GET
    public Response placeNewGazella() {
        placeZooElement(AnimalFactory.createGazella());
        return Response.ok().build();
    }

    /**
     * API to create a new flamingo and place it on a free field
     *
     * @return empty Response object
     */
    @Path("/newFlamingo")
    @GET
    public Response placeNewFlamingo() {
        placeZooElement(AnimalFactory.createFlamingo());
        return Response.ok().build();
    }

    /**
     * API to create a new rose and place it on a free field
     *
     * @return empty Response object
     */
    @Path("/newRose")
    @GET
    public Response placeNewRose() {
        placeZooElement(DecorationFactory.createRose());
        return Response.ok().build();
    }

    /**
     * API to create a new tree and place it on a free field
     *
     * @return empty Response object
     */
    @Path("/newTree")
    @GET
    public Response placeNewTree() {
        placeZooElement(DecorationFactory.createTree());
        return Response.ok().build();
    }

    /**
     * API to create a new gras and place it on a free field
     *
     * @return empty Response object
     */
    @Path("/newGras")
    @GET
    public Response placeNewGras() {
        placeZooElement(GroundFactory.createGras());
        return Response.ok().build();
    }

    /**
     * API to create a new stone and place it on a free field
     *
     * @return empty Response object
     */
    @Path("/newStone")
    @GET
    public Response placeNewStone() {
        placeZooElement(GroundFactory.createStone());
        return Response.ok().build();
    }

    /**
     * API to create a new path
     *
     * @return empty Response object
     */
    @Path("/newPath")
    @GET
    public Response placeNewPath() {
        placeZooElement(GroundFactory.createPath());
        return Response.ok().build();
    }

    /**
     * API to reset the game
     * removes all ZooElements of the field and sets the budget to the start amount
     *
     * @return empty Response object
     */
    @Path("/reset")
    @GET
    public Response resetZoo() {
        HtmlBase.getInstance().reset();
        return Response.ok().build();
    }

    /**
     * API to interact with bought animals
     * feeds animals or earns money of them
     *
     * not really nice but data structure doesn't give a better API
     *
     * @param id of the ZooElement with which should be interacted
     * @return empty Response object
     */
    @Path("/animalAction/{id}")
    @GET
    public Response feedOrEarnAnimal(@PathParam("id") int id) {

        for (int i = 0; i < currentSite.getWidth(); i++) {
            for (int j = 0; j < currentSite.getHeight(); j++) {
                ZooElement element = currentSite.getElement(i, j);
                if (element != null && element.getId() == id && element instanceof Animal) {
                    Animal animal = (Animal) element;
                    if (animal.isHungry() && currentZoo.getMoney() >= 2) {
                        animal.feed();
                        currentZoo.spend(2);
                    }
                    if (animal.isReady()) {
                        currentZoo.earn(animal.collect());
                    }
                }
            }
        }

        return Response.ok().build();
    }

    /**
     * places a new element on the field
     *
     * @param element new ZooElement which should be placed
     */
    private void placeZooElement(ZooElement element) {
        if (currentZoo.getMoney() - element.getCost() >= 0) {
            int tries = 1000;
            boolean success = false;
            while (tries > 0 && !success) {
                int x = rand.nextInt(currentZoo.getSite().getWidth());
                int y = rand.nextInt(currentZoo.getSite().getHeight());
                success = currentZoo.getSite().place(x, y, element);
                --tries;
            }
            if (success) {
                currentZoo.spend(element.getCost());
            }
        }
    }
}
