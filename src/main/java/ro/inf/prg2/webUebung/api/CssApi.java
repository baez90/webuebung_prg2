package ro.inf.prg2.webUebung.api;

import ro.inf.prg2.webUebung.util.ResourceLoader;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

/**
 * API to serve static CSS content
 *
 * uses the helper class ResourceLoader to load static css files from the resource folder
 *
 * @author Peter Kurfer
 * @version 0.1
 */
@Path("/css")
public class CssApi {

    /**
     * loads a file from the 'resource/css' folder with the specified file name
     * <p>
     * content type is set to 'text/css' to tell the browser how he should handle the file content
     *
     * @param fileName name of the css file which should be loaded
     * @return Response object with string representation of the css file
     */
    @GET
    @Path("/{fileName}")
    @Produces("text/css")
    public Response getCssFile(@PathParam("fileName") String fileName) {
        return Response.ok().entity(ResourceLoader.loadResource("css/" + fileName)).build();
    }
}
