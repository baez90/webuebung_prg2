package ro.inf.prg2.webUebung.api;

import ro.inf.prg2.webUebung.util.ResourceLoader;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

/**
 * API to serve static JavaScript content
 *
 * uses the helper class ResourceLoader to load static JavaScript files from the resource folder
 *
 * @author Peter Kurfer
 * @version 0.1
 */
@Path("/js")
public class JsApi {

    /**
     * loads a file from the 'resource/js' folder with the specified file name
     * <p>
     * content type is set to 'application/javascript' which should be correct for HTML5
     *
     * @param fileName name of the js file which should be loaded
     * @return Response object with string representation of the js file
     */
    @GET
    @Produces("application/javascript")
    @Path("/{fileName}")
    public Response getJsFile(@PathParam("fileName") String fileName) {
        return Response.ok().entity(ResourceLoader.loadResource("js/" + fileName)).build();
    }
}
