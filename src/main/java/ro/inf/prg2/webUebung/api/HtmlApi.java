package ro.inf.prg2.webUebung.api;

import ro.inf.prg2.webUebung.web.HtmlBase;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * API to serve HTML content
 *
 * uses the central HtmlBase class to generate the HTML
 *
 * @author Peter Kurfer
 * @version 0.1
 */
@Path("/")
public class HtmlApi {


    /**
     * generates the HTML code from the HtmlBase class
     * <p>
     * content type is 'text/html' if not, the browser won't render it correctly
     *
     * @return Reponse object with string representation of the generated HTML content
     */
    @GET
    @Produces(MediaType.TEXT_HTML)
    public Response getHtmlFile() {
        return Response.ok().entity(HtmlBase.getInstance().toHtmlString()).build();
    }
}
