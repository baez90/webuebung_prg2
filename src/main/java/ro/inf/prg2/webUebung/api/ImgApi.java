package ro.inf.prg2.webUebung.api;

import ro.inf.prg2.webUebung.util.ResourceLoader;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 * API to serve images
 *
 * uses the helper class ResourceLoader to load image binaries from the resource folder
 *
 * @author Peter Kurfer
 * @version 0.1
 */
@Path("/img")
public class ImgApi {


    /**
     * loads a file from 'resource/img' folder with the specified name
     * <p>
     * content type is set to 'image/png' to tell the browser how he should render the image
     * if there are other file types, there might be problems with rendering
     *
     * @param fileName name of the image file which should be loaded
     * @return Response object with the
     */
    @GET
    @Produces("image/png")
    @Path("/{fileName}")
    public byte[] getImgFile(@PathParam("fileName") String fileName) {
        return ResourceLoader.loadBinaryResource("img/" + fileName);
    }
}
