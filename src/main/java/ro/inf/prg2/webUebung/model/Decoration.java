package ro.inf.prg2.webUebung.model;

/**
 * Decorations for the Zoo.
 *
 * @author brm
 */
public class Decoration extends ZooElement {
    private static final long serialVersionUID = 1L;
    private final String type;

    public Decoration(String type, int cost) {
        super(cost);
        // neu f�r �bung03
        if (type == null || type.length() == 0) {
            throw new IllegalArgumentException("type muss ein valider string sein");
        }
        this.type = type;
    }

    public String getType() {
        return this.type;
    }

    @Override
    public void tick() {
        // nothing happens to decorations
    }

    @Override
    public boolean isGround() {
        return false;
    }

    @Override
    public String toHtmlString() {
        return "<img src='/img/decoration" + type + ".png' class='element'>";
    }

    @Override
    public String toString() {
        return "Decoration [type=" + type + ", id=" + getId()
                + ", cost=" + getCost() + "]";
    }

}
