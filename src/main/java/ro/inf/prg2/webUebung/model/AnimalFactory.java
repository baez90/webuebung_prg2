package ro.inf.prg2.webUebung.model;

/**
 * Factory to create Animals.
 *
 * @author brm
 */
public class AnimalFactory {

    // Lion
    private static int nextNumberLion = 1;
    // Gazella
    private static int nextNumberGazella = 1;
    // Flamingo
    private static int nextNumberFlamingo = 1;

    public static Animal createLion(String name) {
        nextNumberLion++;
        return new Animal(GenusSpecies.GENUS_SPECIES_LION, name, 500, 20, 20, 100, new GenusSpecies[]{GenusSpecies.GENUS_SPECIES_GAZELLE, GenusSpecies.GENUS_SPECIES_FLAMINGO});
    }

    public static Animal createLion() {
        return createLion(String.format("Lion%03d", nextNumberLion));
    }

    public static Animal createGazella(String name) {
        nextNumberGazella++;
        return new Animal(GenusSpecies.GENUS_SPECIES_GAZELLE, name, 100, 10, 10, 20, new GenusSpecies[]{});
    }

    public static Animal createGazella() {
        return createGazella(String.format("Gazella%03d", nextNumberGazella));
    }

    public static Animal createFlamingo(String name) {
        nextNumberFlamingo++;
        return new Animal(GenusSpecies.GENUS_SPECIES_FLAMINGO, name, 200, 5, 5, 30, new GenusSpecies[]{});
    }

    public static Animal createFlamingo() {
        return createFlamingo(String.format("Flamingo%03d", nextNumberFlamingo));
    }

    public enum GenusSpecies {

        GENUS_SPECIES_LION("Panthera leo", "Lion"),
        GENUS_SPECIES_GAZELLE("Gazella leptoceros", "Gazelle"),
        GENUS_SPECIES_FLAMINGO("Phoenicopterus jamesi", "Flamingo");

        private String readableType;

        private String type;

        GenusSpecies(String readableType, String type) {
            this.readableType = readableType;
            this.type = type;
        }

        public String getReadableType() {
            return readableType;
        }

        public String getType() {
            return type;
        }
    }

}
