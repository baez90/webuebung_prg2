package ro.inf.prg2.webUebung.model;

/**
 * Generic Tuple to save coordinates in the playground
 * Every Tuple is immutable
 *
 * @author Peter Kurfer
 * @version 0.1
 */
public class Tuple<K, V> {

    private K item1;
    private V item2;

    /**
     * Create a new tuple
     *
     * @param item1
     * @param item2
     */
    public Tuple(K item1, V item2) {
        this.item1 = item1;
        this.item2 = item2;
    }

    public K getItem1() {
        return item1;
    }

    public V getItem2() {
        return item2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tuple<?, ?> tuple = (Tuple<?, ?>) o;

        if (item1 != null ? !item1.equals(tuple.item1) : tuple.item1 != null) return false;
        return !(item2 != null ? !item2.equals(tuple.item2) : tuple.item2 != null);

    }

    @Override
    public int hashCode() {
        int result = item1 != null ? item1.hashCode() : 0;
        result = 31 * result + (item2 != null ? item2.hashCode() : 0);
        return result;
    }
}
