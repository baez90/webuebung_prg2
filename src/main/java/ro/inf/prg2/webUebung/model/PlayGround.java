package ro.inf.prg2.webUebung.model;

import java.util.*;

/**
 * Represents a generic playground where every kind of ZooElement can be placed on
 * uses a list with an inner list to represent the playground
 * additionally a Map is used to save the specific coordinate of every element
 * to be able to get every element by coordinates or object
 * <p>
 * Implements Iterable to be able to use an iterator or the Java8 foreach()
 *
 * @author Peter Kurfer
 * @version 0.1
 */
public class PlayGround<T extends ZooElement> implements Iterable<T> {

    private int width;
    private int height;
    private Map<T, Tuple<Integer, Integer>> elementsOnGround;

    private List<List<T>> ground;

    /**
     * Create a new playground
     *
     * @param width
     * @param height
     */
    public PlayGround(int width, int height) {
        if (width < 1 || height < 1)
            throw new IllegalArgumentException();

        this.width = width;
        this.height = height;

        //initialisieren der ArrayLists
        ground = new ArrayList<>(width);
        for (int i = 0; i < width; i++) {
            ground.add(i, new ArrayList<T>(height));
            for (int j = 0; j < height; j++) {
                ground.get(i).add(j, null);
            }
        }

        /* Elemente koennten sortiert eingefuegt werden aber es wuerde kein nennenswerter Vorteil dadurch entstehen
         * Suchen des Elements sollte auf Basis der HashMap schnell genug sein */
        elementsOnGround = new HashMap<>();
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public T getElement(int x, int y) {
        return ground.get(x).get(y);
    }

    public boolean place(int x, int y, T element) {
        //Pruefung auf bereits positionierte Instanz geht deutlich schneller, da auf dem HashCode gearbeitet wird
        if (elementsOnGround.containsKey(element) || ground.get(x).get(y) != null) {
            return false;
        }

        //Referenz auf neu positioniertes element in beide Container
        ground.get(x).set(y, element);
        elementsOnGround.put(element, new Tuple<>(x, y));
        return true;
    }

    public boolean remove(T element) {
        //Fuer remove muss nicht mehr durch alle Listen iteriert werden, da Koordinaten als Tuple gespeichert werden
        if (element == null || !elementsOnGround.containsKey(element)) {
            return false;
        }
        Tuple<Integer, Integer> coordinates = elementsOnGround.get(element);
        ground.get(coordinates.getItem1()).set(coordinates.getItem2(), null);
        elementsOnGround.remove(element);
        return true;
    }

    /**
     * Tell everyone in the playground that one unit of time has passed.
     */
    public void tick() {
        /* Java 8 Lambda Loesung
        elementsOnGround.keySet().forEach(key -> {
            key.tick();
        });

        noch kuerzere Java 8 Loesung
        elementsOnGround.keySet().forEach(T::tick);*/

        for (T entry : elementsOnGround.keySet()) {
            entry.tick();
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new PlayGroundIterator<>(this);
    }

    /**
     * Iterator implenetation for Iterable interface
     *
     * @param <T> same Type as the playground, so every ZooElement can be iterated
     */
    private static class PlayGroundIterator<T extends ZooElement> implements Iterator<T> {

        private int index;
        private int maxIndex;
        private PlayGround<T> currentGround;
        private T currentElement;

        private PlayGroundIterator(PlayGround<T> ground) {
            currentGround = ground;
            maxIndex = ground.getWidth() * ground.getHeight();
            currentElement = ground.getElement(0, 0);
        }

        @Override
        public boolean hasNext() {
            return currentElement != null;
        }

        @Override
        public T next() {
            T temp = currentElement;
            setToNext();
            return temp;
        }

        @Override
        public void remove() {
            currentGround.remove(currentElement);
        }

        /**
         * Private helper to navigate through the playground
         */
        private void setToNext() {
            if (index < maxIndex - 1) {
                index++;
                /* Teilen mit Rest
                 * ganzzahliger Anteil gibt den Index der aeusseren Liste an
                 * ganzzahliger Rest gibt den Index der inneren Liste an */
                int x = index / currentGround.getWidth();
                int y = index % currentGround.getHeight();
                currentElement = currentGround.getElement(x, y);
            } else {
                currentElement = null;
            }
        }
    }
}
