package ro.inf.prg2.webUebung.model;

import ro.inf.prg2.webUebung.web.IHtmlElement;

import java.io.*;
import java.util.Observable;
import java.util.Observer;

/**
 * Datamodel for the Zoo, i.e. the site and the money we have.
 *
 * @author brm
 */
public class Zoo extends Observable implements Observer, IHtmlElement {
    private Site site;
    private int money;

    /**
     * Create a default Zoo with 10x10 fields and 1000 EUR to start with.
     */
    public Zoo() {
        this(10, 10, 1000);
    }

    public Zoo(int width, int height, int money) {
        this(new Site(width, height), money);
    }

    public Zoo(Site site, int money) {
        this.site = site;
        site.addObserver(this);
        this.money = money;
    }

    /**
     * Read a Zoo from a file.
     *
     * @param filename name of the file to read from.
     * @return a new Zoo Object with the information read from the file.
     * @throws FileNotFoundException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static Zoo read(String filename) throws FileNotFoundException, IOException, ClassNotFoundException {
        return read(new FileInputStream(filename));
    }

    /**
     * Read a Zoo from an InputStream.
     *
     * @param in the stream to read from.
     * @return a new Zoo Object with the information read from the file.
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static Zoo read(InputStream in) throws IOException, ClassNotFoundException {
        ObjectInputStream is = new ObjectInputStream(in);
        Site site = (Site) is.readObject();
        int money = is.readInt();
        is.close();
        return new Zoo(site, money);
    }

    public int getMoney() {
        return this.money;
    }

    /**
     * Earn a certain amount of money.
     *
     * @param amount the amount earned. Has to be >=0.
     * @throws IllegalArgumentException if amount is negative.
     */
    public void earn(int amount) {
        if (amount < 0) {
            throw new IllegalArgumentException("can not earn negative amounts of money");
        }
        this.money += amount;
        setChanged();
        notifyObservers();
    }

    /**
     * Spend a certain amount of money.
     *
     * @param amount the amount spent. Has to be >=0.
     * @throws IllegalArgumentException if not enough money available or the amount <0
     */
    public void spend(int amount) {
        if (amount < 0) {
            throw new IllegalArgumentException("can not spend negative amounts of money");
        }
        if (this.money - amount < 0) {
            throw new IllegalArgumentException("not enough money to spend the given amount");
        }
        this.money -= amount;
        setChanged();
        notifyObservers();
    }

    public Site getSite() {
        return this.site;
    }

    /**
     * Write the Zoo to a file.
     *
     * @param filename name of the file to write to.
     * @throws FileNotFoundException
     * @throws IOException
     */
    public void write(String filename) throws FileNotFoundException, IOException {
        write(new FileOutputStream(filename));
    }

    /**
     * Write the Zoo to a given OutputStream.
     *
     * @param out the stream to write to.
     * @throws IOException
     */
    public void write(OutputStream out) throws IOException {
        ObjectOutputStream os = new ObjectOutputStream(out);
        os.writeObject(site);
        os.writeInt(money);
        ;
        os.close();
    }

    @Override
    public void update(Observable arg0, Object arg1) {
        setChanged();
        notifyObservers();
    }

    @Override
    public String toHtmlString() {
        return site.toHtmlString() + "<p>Kontostand: " + money + "</p>";
    }
}
