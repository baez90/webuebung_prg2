package ro.inf.prg2.webUebung.model;

public class Util {
    /**
     * Utility function to get a (unique) one character identifier for
     * each ZooElement. A little hacky, a better solution would be to
     * use the Visitor Pattern, but this has not been introduced yet.
     *
     * @param element a ZooElement
     * @return a unique one character identifier.
     */
    public static char getCharIdentifier(ZooElement element) {
        if (element == null) return ' ';

        if (element.getClass() == Animal.class) {
            Animal animal = (Animal) element;
            switch (animal.getGenusSpecies()) {
                case GENUS_SPECIES_LION:
                    return 'L';
                case GENUS_SPECIES_GAZELLE:
                    return 'G';
                case GENUS_SPECIES_FLAMINGO:
                    return 'F';
                default:
                    return 'A';
            }
        }
        if (element.getClass() == Decoration.class) {
            Decoration decoration = (Decoration) element;
            switch (decoration.getType()) {
                case DecorationFactory.DECO_TYPE_ROSE:
                    return 'r';
                case DecorationFactory.DECO_TYPE_TREE:
                    return 't';
                default:
                    return 'd';
            }
        }
        ;
        if (element.getClass() == Ground.class) {
            Ground ground = (Ground) element;
            switch (ground.getType()) {
                case GroundFactory.GROUND_TYPE_GRAS:
                    return 'g';
                case GroundFactory.GROUND_TYPE_PATH:
                    return 'p';
                case GroundFactory.GROUND_TYPE_STONE:
                    return 's';
                default:
                    return '*';
            }
        }

        return '?';
    }
}
