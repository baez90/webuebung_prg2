package ro.inf.prg2.webUebung.model;

import java.util.Arrays;

/**
 * Animal in the zoo. Animals have 4 states:
 * "hungry": they stay hungry until the are fed, then they move into "digesting"
 * "digesting": after "timeDigesting" they move into "ready".
 * "ready": you can collect their money in this state. they stay in this state for 10 units, than they move into "sleeping"
 * "sleeping": after "timeSleeping" they move into "hungry".
 * <p>
 * Newly created animals start out sleeping.
 *
 * @author brm
 */
public class Animal extends ZooElement {
    private static final long serialVersionUID = 1L;
    private final static int TIME_READY = 10;
    private final AnimalFactory.GenusSpecies genusSpecies; // kind of Animal, e.g. Panthera Leo
    private String name; // name of Animal, e.g. Ludwig, Gisela
    private int timeDigesting;
    private int timeSleeping;
    private int collectionAmount;
    private int remainingTime;
    private AnimalState state;
    private AnimalFactory.GenusSpecies[] devours;

    public Animal(AnimalFactory.GenusSpecies genusSpecies, String name, int cost, int timeDigesting, int timeSleeping, int collectionAmount, AnimalFactory.GenusSpecies[] devours) {
        super(cost);
        if (name == null || name.length() == 0) {
            throw new IllegalArgumentException("name muss ein valider String sein");
        }
        if (timeDigesting <= 0) {
            throw new IllegalArgumentException("timeDigesting muss >0 sein");
        }
        if (timeSleeping <= 0) {
            throw new IllegalArgumentException("timeSleeping muss >0 sein");
        }
        if (collectionAmount < 0) {
            throw new IllegalArgumentException("collectionAmount muss >=0 sein");
        }
        if (devours == null) {
            throw new IllegalArgumentException("devours darf nicht ==null sein");
        }

        this.genusSpecies = genusSpecies;
        this.name = name;
        this.devours = Arrays.copyOf(devours, devours.length); // make a copy for safety reasons
        Arrays.sort(this.devours); // sort to make searching below easier
        this.timeDigesting = timeDigesting;
        this.timeSleeping = timeSleeping;
        this.collectionAmount = collectionAmount;
        this.remainingTime = timeSleeping;
        this.state = AnimalState.SLEEPING;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AnimalFactory.GenusSpecies getGenusSpecies() {
        return this.genusSpecies;
    }

    public int getCollectionAmount() {
        return this.collectionAmount;
    }

    // neu f�r �bung04
    public int getTimeDigesting() {
        return this.timeDigesting;
    }

    // neu f�r �bung04
    public int getTimeSleeping() {
        return this.timeSleeping;
    }

    // neu f�r �bung04
    public int getTimeReady() {
        return TIME_READY;
    }

    public boolean isHungry() {
        return state == AnimalState.HUNGRY;
    }

    public boolean isReady() {
        return state == AnimalState.READY;
    }

    public boolean isSleeping() {
        return state == AnimalState.SLEEPING;
    }

    public boolean isDigesting() {
        return state == AnimalState.DIGESTING;
    }

    /**
     * Feed the animal. If it not hungry, nothing happens. If it was hungry, it is now digesting.
     *
     * @return true if the animal was hungry, false if it was not.
     */
    public boolean feed() {
        if (isHungry()) {
            state = AnimalState.DIGESTING;
            remainingTime = timeDigesting;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Collect the reward from the animal. If it is not ready, you get 0. If it is ready, you get the amount
     * and the animals falls asleep.
     *
     * @return
     */
    public int collect() {
        if (isReady()) {
            state = AnimalState.SLEEPING;
            remainingTime = timeSleeping;
            return collectionAmount;
        } else {
            return 0;
        }
    }

    /**
     * One unit of time has passed. If the remaining time is zero, need to do a state transition.
     * State: !hungry && !ready --> move to hungry, !ready.
     * State: !hungry && ready --> move to hungry, !ready.
     * State: hungry -->
     */
    @Override
    public void tick() {
        switch (state) {
            case HUNGRY:
                break;
            case DIGESTING:
                if (--remainingTime == 0) {
                    state = AnimalState.READY;
                    remainingTime = TIME_READY;
                }
                break;
            case READY:
                if (--remainingTime == 0) {
                    state = AnimalState.SLEEPING;
                    remainingTime = timeSleeping;
                }
                break;
            case SLEEPING:
                if (--remainingTime == 0) {
                    state = AnimalState.HUNGRY;
                }
                break;
        }
    }

    @Override
    public boolean isGround() {
        return false;
    }

    /**
     * Does this animal eat this other animal?
     *
     * @param other the other animal
     * @return true or false.
     */
    public boolean devours(Animal other) {
        return (Arrays.binarySearch(this.devours, other.getGenusSpecies()) >= 0);
    }

    @Override
    public String toString() {
        return "Animal [name=" + name + ", genusSpecies=" + genusSpecies
                + ", timeDigesting=" + timeDigesting + ", timeSleeping="
                + timeSleeping + ", collectionAmount=" + collectionAmount
                + ", remainingTime=" + remainingTime + ", state=" + state.readableState
                + ", devours=" + Arrays.toString(devours) + ", id="
                + getId() + ", cost=" + getCost() + "]";
    }

    @Override
    public String toHtmlString() {
        return "<img src='/img/animal" + genusSpecies.getType() + state.getReadableState() + ".png' onclick=\"clickHandler('/link/animalAction/" + getId() + "')\" class='element'>";
    }

    public enum AnimalState {

        HUNGRY("Hungry"),
        DIGESTING("Digesting"),
        SLEEPING("Sleeping"),
        READY("Ready");

        private String readableState;

        AnimalState(String state) {
            readableState = state;
        }

        public String getReadableState() {
            return readableState;
        }

    }


}
