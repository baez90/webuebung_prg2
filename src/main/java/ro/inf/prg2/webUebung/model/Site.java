package ro.inf.prg2.webUebung.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import ro.inf.prg2.webUebung.web.IHtmlElement;

import java.io.Serializable;
import java.util.Observable;

/**
 * The actual Zoo site. A rectangular field of 'squares', where each spot can hold
 * both a ground element and a non-ground element (e.g. animal, decoration) sitting
 * on top of this.
 *
 * @author brm
 */
public class Site extends Observable implements Serializable, IHtmlElement {
    private static final long serialVersionUID = 1L;
    private int width;
    private int height;
    private PlayGround<ZooElement> elements;
    private PlayGround<Ground> grounds;

    /**
     * Make a new site with the given width and height.
     *
     * @param width
     * @param height
     */
    public Site(int width, int height) {
        if (width < 1 || height < 1)
            throw new IllegalArgumentException();
        this.width = width;
        this.height = height;
        elements = new PlayGround<>(width, height);
        grounds = new PlayGround<>(width, height);
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }

    public ZooElement getGround(int x, int y) {
        return grounds.getElement(x, y);
    }

    public ZooElement getElement(int x, int y) {
        return elements.getElement(x, y);
    }

    /**
     * Add the element at the given position. Only succeeds if the element is
     * not already in the Site and it can be placed (i.e. is ground and there is
     * no other ground at this position or is non-ground and there is no other
     * non-ground at this position).
     *
     * @param x       x-position to add at.
     * @param y       y-position to add at.
     * @param element the element to add.
     * @return true if successful, false otherwise.
     */
    public boolean place(int x, int y, ZooElement element) {
        return element.isGround() ? grounds.place(x, y, (Ground) element) : elements.place(x, y, element);
    }

    /**
     * Remove a ZooElement from the Site.
     *
     * @param element the element to remove.
     * @return true if removed, false if not (or not found).
     */
    public boolean remove(ZooElement element) {
        return element.isGround() ? grounds.remove((Ground) element) : elements.remove(element);
    }

    /**
     * Tell everyone in the site that one unit of time has passed.
     */
    public void tick() {
        elements.tick();
        setChanged();
        notifyObservers();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("width", width)
                .append("height", height)
                .append("elements", elements)
                .append("grounds", grounds)
                .toString();
    }

    @Override
    public String toHtmlString() {
        StringBuilder htmlBuilder = new StringBuilder();
        htmlBuilder.append("<table>");

        for (int i = 0; i < grounds.getWidth(); i++) {
            htmlBuilder.append("<tr>");
            for (int j = 0; j < grounds.getHeight(); j++) {
                htmlBuilder.append("<td>");
                if (grounds.getElement(i, j) != null) {
                    htmlBuilder.append(grounds.getElement(i, j).toHtmlString());
                }
                if (elements.getElement(i, j) != null) {
                    htmlBuilder.append(elements.getElement(i, j).toHtmlString());
                }
                htmlBuilder.append("</td>");
            }
            htmlBuilder.append("</tr>");
        }

        htmlBuilder.append("</table>");
        return htmlBuilder.toString();
    }
}
