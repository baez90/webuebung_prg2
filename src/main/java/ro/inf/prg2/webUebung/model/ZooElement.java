package ro.inf.prg2.webUebung.model;

import ro.inf.prg2.webUebung.web.IHtmlElement;

import java.io.Serializable;


/**
 * Abstract base class for all visual elements in the zoo.
 *
 * @author brm
 */
public abstract class ZooElement implements Comparable<ZooElement>, Serializable, IHtmlElement {
    private static final long serialVersionUID = 1L;
    private static int nextId = 1;
    private final int id;
    private int cost;

    public ZooElement(int cost) {
        // neu f�r �bung03
        if (cost < 0) {
            throw new IllegalArgumentException("Kosten m�ssen >=0 sein");
        }
        this.id = nextId++;
        this.cost = cost;
    }

    public int getId() {
        return this.id;
    }

    /**
     * Cost of this element.
     *
     * @return amount.
     */
    public int getCost() {
        return this.cost;
    }

    /**
     * Notify this element that one unit of time has passed.
     */
    public abstract void tick();

    /**
     * Is this a Ground element, or something that can be placed on Ground?
     */
    public abstract boolean isGround();

    @Override
    public abstract String toHtmlString();

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ZooElement other = (ZooElement) obj;
        if (id != other.id)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "ZooElement [id=" + id + ", cost=" + cost + "]";
    }

    @Override
    public int compareTo(ZooElement other) {
        // kurze, elegante L�sung:
        return this.id - other.id;

        // auch korrekt:
//		if(this.id < other.id) {
//			return -1;
//		} else if(this.id > other.id) {
//			return +1;
//		} else {
//			return 0;
//		}
    }
}
