package ro.inf.prg2.webUebung.model;

/**
 * Different kinds of ground for the Zoo.
 *
 * @author brm
 */
public class Ground extends ZooElement {
    private static final long serialVersionUID = 1L;
    private final String type;

    public Ground(String type, int cost) {
        super(cost);
        // neu f�r �bung03
        if (type == null || type.length() == 0) {
            throw new IllegalArgumentException("type muss ein valider string sein");
        }
        this.type = type;
    }

    public String getType() {
        return this.type;
    }

    @Override
    public void tick() {
        // nothing happens to ground
    }

    @Override
    public boolean isGround() {
        return true;
    }

    @Override
    public String toHtmlString() {
        return "<img src='/img/ground" + type + ".png' class='ground'>";
    }

    @Override
    public String toString() {
        return "Ground [type=" + type + ", id=" + getId() + ", cost="
                + getCost() + "]";
    }
}
