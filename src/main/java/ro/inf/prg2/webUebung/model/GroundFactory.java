package ro.inf.prg2.webUebung.model;

/**
 * Factory to create different grounds.
 *
 * @author brm
 */
public class GroundFactory {
    public static final String GROUND_TYPE_GRAS = "Gras";
    public static final String GROUND_TYPE_PATH = "Path";
    public static final String GROUND_TYPE_STONE = "Stone";

    public static Ground createGras() {
        return new Ground(GROUND_TYPE_GRAS, 10);
    }

    public static Ground createPath() {
        return new Ground(GROUND_TYPE_PATH, 15);
    }

    public static Ground createStone() {
        return new Ground(GROUND_TYPE_STONE, 5);
    }

}
