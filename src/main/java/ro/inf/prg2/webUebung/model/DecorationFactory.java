package ro.inf.prg2.webUebung.model;

/**
 * Factory to create Decorations.
 *
 * @author brm
 */
public class DecorationFactory {
    public static final String DECO_TYPE_ROSE = "Rose";
    public static final String DECO_TYPE_TREE = "Tree";

    public static Decoration createRose() {
        return new Decoration(DECO_TYPE_ROSE, 100);
    }

    public static Decoration createTree() {
        return new Decoration(DECO_TYPE_TREE, 200);
    }
}
