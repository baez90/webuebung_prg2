package ro.inf.prg2.webUebung.web;

import ro.inf.prg2.webUebung.model.Zoo;
import ro.inf.prg2.webUebung.util.ResourceLoader;

import java.util.HashMap;
import java.util.Map;

/**
 * Central class for handling HTML content
 * concatenates different IHtmlElement objects to one document
 *
 * HtmlBase is implemented as Singleton because REST doesn't know a session or anything corresponding so it has to be static
 *
 * @author Peter Kurfer
 * @version 0.1
 */
public class HtmlBase implements IHtmlElement {

    private static int ZOO_HEIGHT = 10;
    private static int ZOO_WIDTH = 10;
    private static int START_BUDGET = 2500;
    private static String CLOSING_TAG = "</body></html>";
    private static int latestIndex = 0;

    //Singleton instance has to be the latest static element to ensure that all other variables are initialized
    private static HtmlBase instance = new HtmlBase();

    /**
     * content of the base html template as String
     */
    private String htmlBase;

    /**
     * file name of the html template
     */
    private String templateName;
    private Zoo zoo;

    /**
     * Map to ensure that all elements are rendered in the correct order in which they're added
     */
    private Map<Integer, IHtmlElement> htmlElements;

    private HtmlBase() {
        zoo = new Zoo(ZOO_WIDTH, ZOO_HEIGHT, START_BUDGET);
        htmlElements = new HashMap<>();
        templateName = "default.html";
        initBuilder();
    }

    /**
     * Singleton instance of HtmlBase
     *
     * @return HtmlBase instance
     */
    public static HtmlBase getInstance() {
        return instance;
    }

    public void addHtmlElement(IHtmlElement element) {
        htmlElements.put(latestIndex++, element);
    }

    /**
     * Resets the game
     */
    public void reset() {
        zoo = new Zoo(ZOO_WIDTH, ZOO_HEIGHT, START_BUDGET);
        latestIndex = 0;
        htmlElements.clear();
        initBuilder();
        addHtmlElement(zoo);
    }

    /**
     * loads a different template file as 'default.html'
     * @param templateFileName name of a template file which should be loaded from the 'resources/web' folder
     */
    public void load(String templateFileName) {
        templateName = templateFileName;
        initBuilder();
    }

    public Zoo getZoo() {
        return zoo;
    }

    private void initBuilder() {
        htmlBase = ResourceLoader.loadResource("web/" + templateName);
    }

    /**
     * concatenates the template file with all following IHtmlElements from the map and closes the <html> and the <body> tag
     *
     * @return completed HTML view to get delivered
     */
    @Override
    public String toHtmlString() {
        StringBuilder htmlBuilder = new StringBuilder();
        htmlBuilder.append(htmlBase);

        if (htmlElements.size() > 0) {
            for (int i = 0; i < htmlElements.size(); i++) {
                htmlBuilder.append(htmlElements.get(i).toHtmlString());
            }
        }
        //Schliessen des <body> und des <html> Tags
        htmlBuilder.append(CLOSING_TAG);
        return htmlBuilder.toString();
    }
}
