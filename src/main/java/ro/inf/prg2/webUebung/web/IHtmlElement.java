package ro.inf.prg2.webUebung.web;

/**
 * Has to be implemented if a element should get added to the HtmlBase
 *
 * @author Peter Kurfer
 * @version 0.1
 */
public interface IHtmlElement {

    /**
     * Get's called when a new HTML view is rendered in the HtmlBase
     *
     * @return HTML representation of the implementing element
     */
    String toHtmlString();
}
