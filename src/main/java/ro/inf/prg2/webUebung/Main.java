package ro.inf.prg2.webUebung;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import ro.inf.prg2.webUebung.api.*;
import ro.inf.prg2.webUebung.web.HtmlBase;

import java.io.IOException;
import java.net.URI;
import java.util.HashSet;
import java.util.Set;

/**
 * Main Class which starts the grizzly HTTP server
 *
 * All HTML-Elements which should be included in the HTML view should be added here to the HtmlBase
 *
 * @author Peter Kurfer
 * @version 0.1
 */
public class Main {

    public static void main(String[] args) throws IOException {
        //load different html template:
        HtmlBase.getInstance().load("styled.html");

        /*TODO add html elements to HtmlBase for displaying
        example: */
        HtmlBase.getInstance().addHtmlElement(HtmlBase.getInstance().getZoo());

        //use local method to get all api classes
        //HttpServer server = GrizzlyHttpServerFactory.createHttpServer(URI.create("http://localhost:9998"), new ResourceConfig(getServiceClasses()));

        //use reflection library to get all api classes
        HttpServer server = GrizzlyHttpServerFactory.createHttpServer(URI.create("http://localhost:9998"), new ResourceConfig(ApiRegistry.ApiClasses));
        server.start();

        //just to let process run and wait for keyboard input
        System.in.read();
    }

    /**
     * All api classes should get registered here
     *
     * @return set of all api classes which get loaded by grizzly and jersey
     */
    private static Set<Class<?>> getServiceClasses() {
        Set<Class<?>> classes = new HashSet<>();
        classes.add(CssApi.class);
        classes.add(HtmlApi.class);
        classes.add(JsApi.class);
        classes.add(ImgApi.class);
        classes.add(HyperlinksApi.class);
        //add resource classes
        return classes;
    }
}
