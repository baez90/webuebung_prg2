/**
 * Script reloads the page every 5 seconds to get updates like animal is ready now and so on
 * furthermore it implements an http call to the tick-link to get game dynamic running
 *
 * @author Peter Kurfer
 */

/**
 * calls every second one tick
 */
setTimeout(function () {
    tick();
}, 1000);

/**
 * triggers a reload every 3 seconds
 * it's a little bit complicated to get the correct balance
 */
setTimeout(function () {
    location.reload();
}, 3000);

/**
 * function to call API and trigger a tick for lifecycle of the ZooElements
 */
function tick() {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", "/link/tick", true);
    xmlHttp.send();
}

/**
 * function to ease click handling
 * calls the specified API
 * @param target URI to the API which should be called
 */
function clickHandler(target) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", target, true);
    xmlHttp.send();
    location.reload();
}